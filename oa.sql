/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : oa

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-06-30 20:07:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_advice
-- ----------------------------
DROP TABLE IF EXISTS `tb_advice`;
CREATE TABLE `tb_advice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `createDate` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_advice
-- ----------------------------
INSERT INTO `tb_advice` VALUES ('6', '地点：暂定香格里拉', '2017-12-27 09:21:13', '周三聚会', '1');
INSERT INTO `tb_advice` VALUES ('9', 'ssss', '2017-12-26 17:21:17', 'ssss', '1');
INSERT INTO `tb_advice` VALUES ('11', '777', '2017-12-26 17:21:46', '77', '1');

-- ----------------------------
-- Table structure for tb_dept
-- ----------------------------
DROP TABLE IF EXISTS `tb_dept`;
CREATE TABLE `tb_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dept
-- ----------------------------
INSERT INTO `tb_dept` VALUES ('1', '信息安全部', '负责网络信息的搜集');
INSERT INTO `tb_dept` VALUES ('5', '项目开发部', '完成对目标系统的功能、性能、接口、界面等方面的设计要求。 \n ▪ 按《软件系统详细设计说明书》进行代码实现\n ▪  测试编写完成的软件，逐个确认每个功能是否符合《软件功能详细设计说明书》中的要求。根据软件技术部提供的测试报告修改软件BUG');
INSERT INTO `tb_dept` VALUES ('6', '市场营销部', '分析市场状况，编写《销售预测报告》  ▪ 拟定年度销售计划，根据当前发展计划合理进行人员配备 ▪ 在项目前期根据《需求调研计划》对客户进行需求调研');
INSERT INTO `tb_dept` VALUES ('7', '软件技术部', '编写测试用例，按测试用例进行测试工作并编写《测试报告》 \n▪ 完成目标项目的《操作手册》和相关培训文档的编写 \n▪ 负责项目的质量审查工作 ▪ 负责制定项目实施计划');
INSERT INTO `tb_dept` VALUES ('8', '系统详细设计', '完成对目标系统的功能、性能、接口、界面等方面的设计要求。 ▪ 按《软件系统详细设计说明书》进行代码实现。  ▪  测试编写完成的软件，逐个确认每个功能是否符合《软件功能详细设计说明书》中的要求。根据软件技术部提供的测试报告修改软件BUG');
INSERT INTO `tb_dept` VALUES ('9', '系统详细', '拟定年度销售计划，根据当前发展计划合理进行人员配备 ▪ 在项目前期根据《需求调研计划》对客户进行需求调研 ▪ 配合软件技术部收集整理客户需求工作中有关的所有事项 ▪ 负责《用户需求说明书》得到用户的认可与');
INSERT INTO `tb_dept` VALUES ('10', '22', '22');
INSERT INTO `tb_dept` VALUES ('11', '11', '11');
INSERT INTO `tb_dept` VALUES ('12', '22', '22');
INSERT INTO `tb_dept` VALUES ('13', '111', '11');
INSERT INTO `tb_dept` VALUES ('14', '11', '111');
INSERT INTO `tb_dept` VALUES ('15', '111', '111');
INSERT INTO `tb_dept` VALUES ('16', '1', '1');
INSERT INTO `tb_dept` VALUES ('17', '11', '11111');
INSERT INTO `tb_dept` VALUES ('18', '11', '1');
INSERT INTO `tb_dept` VALUES ('19', '1', '111');
INSERT INTO `tb_dept` VALUES ('20', '111', null);

-- ----------------------------
-- Table structure for tb_doc
-- ----------------------------
DROP TABLE IF EXISTS `tb_doc`;
CREATE TABLE `tb_doc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `fileName` varchar(100) DEFAULT NULL,
  `remark` text,
  `title` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqotp8dpcld7dkdg84wnh0sbyn` (`uid`),
  CONSTRAINT `FKqotp8dpcld7dkdg84wnh0sbyn` FOREIGN KEY (`uid`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_doc
-- ----------------------------
INSERT INTO `tb_doc` VALUES ('12', '2017-12-27 15:15:53', 'UserloginServiceImplTest.java--0931d179-179f-41e2-95a4-3d0115949682', '数据库表', 'mysq', '1');
INSERT INTO `tb_doc` VALUES ('13', '2017-12-27 15:16:33', 'StudentServiceImplTest.java--d90709c9-b99e-4805-85be-3429ed432e44', 'Testdemo', 'office', '1');

-- ----------------------------
-- Table structure for tb_employee
-- ----------------------------
DROP TABLE IF EXISTS `tb_employee`;
CREATE TABLE `tb_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(100) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `cardId` varchar(100) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `education` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `hobby` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `party` varchar(10) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `postCode` varchar(10) DEFAULT NULL,
  `qqNum` varchar(12) DEFAULT NULL,
  `race` varchar(20) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `jid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ej` (`jid`),
  KEY `fk_ed` (`did`),
  CONSTRAINT `fk_ed` FOREIGN KEY (`did`) REFERENCES `tb_dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ej` FOREIGN KEY (`jid`) REFERENCES `tb_job` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_employee
-- ----------------------------
INSERT INTO `tb_employee` VALUES ('1', '重庆市-县-巫山县', '2017-12-02 00:00:00', '342401199703123015', '2017-12-22 20:50:59', '本科', '13063310938@163.com', '0', '打球', '程克俊', '4', '13063310938', '123456', '1396987477', '汉', '打球1', '软件工程', '1', '4', '0');
INSERT INTO `tb_employee` VALUES ('5', '四川省-德阳市-中江县', '2017-12-02 00:00:00', '342401199703123016', '2017-12-22 21:38:56', '博士', '13063310938@163.com', '0', '啊啊啊', '张三丰', '党员', '13063310938', '123456', '112', '汉族', '啊啊啊啊', '密码学', '6', '11', '0');
INSERT INTO `tb_employee` VALUES ('6', '湖北省-十堰市-张湾区', '2017-12-08 00:00:00', '342401199703123015', '2017-12-23 09:26:04', '博士', '13063310938@163.com', '0', '看漫画', '记得要让着本宝宝', '团员', '13063310938', null, '1396987477', '汉', '看电影', '密码学', '1', '11', null);

-- ----------------------------
-- Table structure for tb_job
-- ----------------------------
DROP TABLE IF EXISTS `tb_job`;
CREATE TABLE `tb_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `remanrk` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_job
-- ----------------------------
INSERT INTO `tb_job` VALUES ('1', '高级程序员', '项目开发');
INSERT INTO `tb_job` VALUES ('4', '产品经理', '配合市场部完成项目需求调研');
INSERT INTO `tb_job` VALUES ('6', '测试经理', '了解项目的基本流程，可以');
INSERT INTO `tb_job` VALUES ('7', '11', '111');
INSERT INTO `tb_job` VALUES ('8', '11', '111');
INSERT INTO `tb_job` VALUES ('9', '11', '111');
INSERT INTO `tb_job` VALUES ('10', '11', '111');
INSERT INTO `tb_job` VALUES ('11', '11', '111');
INSERT INTO `tb_job` VALUES ('12', '1111', '111');
INSERT INTO `tb_job` VALUES ('13', '11111', '111');
INSERT INTO `tb_job` VALUES ('14', '11111', '111');
INSERT INTO `tb_job` VALUES ('15', '11111', '111');

-- ----------------------------
-- Table structure for tb_sign
-- ----------------------------
DROP TABLE IF EXISTS `tb_sign`;
CREATE TABLE `tb_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createTime` datetime DEFAULT NULL,
  `flag` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlll66hyar28qupsy32umn3bms` (`uid`),
  CONSTRAINT `FKlll66hyar28qupsy32umn3bms` FOREIGN KEY (`uid`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_sign
-- ----------------------------
INSERT INTO `tb_sign` VALUES ('1', '2017-12-13 20:09:44', '1', '1');
INSERT INTO `tb_sign` VALUES ('2', '2017-12-20 20:09:57', '0', '3');
INSERT INTO `tb_sign` VALUES ('3', '2017-12-04 21:10:51', '1', '3');
INSERT INTO `tb_sign` VALUES ('4', '2017-12-13 21:18:43', '1', '5');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` date DEFAULT NULL,
  `loginname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', '2017-12-21', 'ckj', '123456', '1', 'ckj');
INSERT INTO `tb_user` VALUES ('3', '2017-12-27', '侠客', '123456', '0', '侠客');
INSERT INTO `tb_user` VALUES ('5', '2017-12-27', 'admin', '123456', '1', 'ad');
