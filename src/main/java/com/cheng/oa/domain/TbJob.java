package com.cheng.oa.domain;

import lombok.Data;

@Data
public class TbJob {
    private Integer id;

    private String  name;

    private String  remanrk;

    @Override
    public String toString() {
        return "TbJob [id=" + id + ", name=" + name + ", remanrk=" + remanrk + "]";
    }

}
