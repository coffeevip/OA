package com.cheng.oa.domain;

import lombok.Data;

import java.util.Date;

@Data
public class TbUser {
    private Integer id;

    private Date    createdate;

    private String  loginname;

    private String  password;

    private Integer status;

    private String  username;

    @Override
    public String toString() {
        return "TbUser [id=" + id + ", createdate=" + createdate + ", loginname=" + loginname + ", password=" + password
                + ", status=" + status + ", username=" + username + "]";
    }

}
