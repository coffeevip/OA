package com.cheng.oa.domain;

import lombok.Data;

import java.util.Date;

@Data
public class TbSign {
    private Integer id;

    private Date    createtime;

    private Integer flag;

    private Integer uid;

    private TbUser  user;

}
