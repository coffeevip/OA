/**
 * 
 */
package com.cheng.oa.vo;

import lombok.Data;

@Data
public class SingChart {

    private String day;

    private long   num;

}
