package com.cheng.oa.serviceImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cheng.oa.domain.TbDept;
import com.cheng.oa.domain.TbDeptExample;
import com.cheng.oa.mapper.TbDeptMapper;
import com.cheng.oa.service.DeptService;
import com.cheng.oa.vo.PageBean;

@Service
@Slf4j
public class DeptServiceImpl implements DeptService {

    @Autowired
    private TbDeptMapper deptMapper;

    @Override
    public PageBean<TbDept> showDept(Integer pageNumber, Integer pageSize, String dname) {
        System.out.println(dname);

        if (!"".equals(dname)) {
            // select * from tb_dept where name=#{name} limit #{rowStart},#{rowEnd}
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("name", "%" + dname + "%");
            int rowStart = (pageNumber - 1) * pageSize;
            map.put("rowStart", rowStart);
            map.put("rowEnd", pageSize);
            List<TbDept> tbDepts = deptMapper.selectPageByName(map);
            PageBean<TbDept> pageBean = new PageBean<TbDept>();
            pageBean.setRows(tbDepts);
            Integer total = deptMapper.selectLikeCount("%" + dname + "%");
            pageBean.setTotal(total);
            return pageBean;
        } else {
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            int rowStart = (pageNumber - 1) * pageSize;
            map.put("rowStart", rowStart);
            map.put("rowEnd", pageSize);
            List<TbDept> tbDepts = deptMapper.selectPage(map);
            PageBean<TbDept> pageBean = new PageBean<TbDept>();
            pageBean.setRows(tbDepts);
            TbDeptExample example = new TbDeptExample();
            List<TbDept> tball = deptMapper.selectByExample(example);
            pageBean.setTotal(tball.size());
            return pageBean;
        }
    }

    @Override
    public boolean addDept(TbDept dept) {
        return deptMapper.insertSelective(dept) > 0;

    }

    @Override
    public boolean deleteById(Integer id) {
        return deptMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean deleteBybatch(Integer[] ids) {
        TbDeptExample example = new TbDeptExample();
        example.createCriteria().andIdIn(Arrays.asList(ids));
        return deptMapper.deleteByExample(example) > 0;
    }

    @Override
    public TbDept findDeptById(Integer id) {
        TbDept selectByPrimaryKey = deptMapper.selectByPrimaryKey(id);
        return selectByPrimaryKey;
    }

    @Override
    public boolean updateDept(TbDept dept) {
        TbDeptExample example = new TbDeptExample();
        example.createCriteria().andIdEqualTo(dept.getId());
        return deptMapper.updateByExampleSelective(dept, example) > 0;

    }

}
